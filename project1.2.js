class Main {
    static main() {
        let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        console.log(this.isThereInMass(arr, 11));
    }

    static isThereInMass(arr, i) {
        for (let j = 0; j < arr.length; j++) {
            if (arr[j] === i) {
                return true;
            }
        }
        return false;
    }
}
Main.main()